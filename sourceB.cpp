#include "sqlite.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;


unordered_map<string, vector<string>> results;

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;

	rc = sqlite3_open("carsDealer.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
	system("CLS");
}


bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	int rc, CarPrice, BuyerM, IfE;
	bool flage= true;
	
	char* Available;

	char* carID;
	itoa(carid, carID, 10);
	char* Cid;
	strcpy(Cid, "SELECT price FROM cars WHERE id = ");
	strcat(Cid, carID);

	cout << Cid << endl;
	rc = sqlite3_exec(db, Cid, callback, &CarPrice, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}


	strcpy(Available, "SELECT available FROM cars WHERE id = ");
	strcat(Available, carID);

	cout << Available << endl;
	rc = sqlite3_exec(db, Available, callback, &IfE, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	if (IfE!=1)
	{
		flage = false;
	}
	else
	{
		char* buyerID;
		itoa(buyerid, buyerID, 10);
		char* Bid;
		strcpy(Bid, "SELECT balance FROM accounts WHERE id = ");
		strcat(Bid, buyerID);

		cout << Bid << endl;
		rc = sqlite3_exec(db, Bid, callback, &BuyerM, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}

		if (!(BuyerM >= CarPrice))
		{
			flage = false;
		}
	}

	
	return(flage);
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc, getFrom, giveTo;
	char *ErrMsg = 0;
	char* From;
	char* To;
	char* pFrom;
	
	bool flage;

	itoa(from, From, 10);
	strcpy(pFrom, "SELECT balance FROM accounts WHERE id =");
	strcat(pFrom, From);
	

	rc = sqlite3_exec(db, pFrom, callback, &getFrom, &giveTo);

	if ((getFrom - amount) < 0)
	{
		flage = false;
	}

	else
	{
		strcpy(pFrom, "UPDATE accounts SET balance =");
		itoa((getFrom - amount), From, 10);
		strcat(pFrom, From);
		
		strcat(pFrom, " WHERE id=");
		itoa(from, From, 10);
		strcat(pFrom, From);
		strcat(pFrom, ";");

		rc = sqlite3_exec(db, pFrom, NULL, 0, &ErrMsg);

		if (rc)
		{
			cout << endl << "sorry I can't open your file..." << sqlite3_errmsg(db) << endl;
			sqlite3_close(db);
			return(1);
		}

		itoa(to, To, 10);
		strcpy(pFrom, "SELECT balance FROM accounts WHERE id=");
		strcat(pFrom, To);
		strcat(pFrom, ";");

		rc = sqlite3_exec(db, pFrom, callback, &giveTo, &ErrMsg);

		if (rc)
		{
			cout << endl << "Sorry, File can not open." << sqlite3_errmsg(db) << endl;
			sqlite3_close(db);
			return(1);
		}

		strcpy(pFrom, "UPDATE accounts SET balance=");
		itoa((giveTo + amount), To, 10);
		strcat(pFrom, To);

		strcat(pFrom, " WHERE id=");
		itoa(to, To, 10);
		strcat(pFrom, To);
		strcat(pFrom, ";");

		rc = sqlite3_exec(db, pFrom, NULL, 0, &ErrMsg);

		if (rc)
		{
			cout << endl << "Sorry, File can not open." << sqlite3_errmsg(db) << endl;
			sqlite3_close(db);
			return(1);
		}

		flage = true;
	}

	return(flage);
}


int callback(void *data, int argc, char **argv, char **azColName)
{
	string value;

	*static_cast<int*>(data) = atoi(argv[0]);

	return(0);
}
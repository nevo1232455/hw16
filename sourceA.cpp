#include "sqlite.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;


unordered_map<string, vector<string>> results;

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;

	rc = sqlite3_open("Firstpart.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
	system("CLS");


	cout << "CREATE table people(Id integer autoincrement, Name VARCHAR)" << endl;

	rc = sqlite3_exec(db, "CREATE table people(Id integer autoincrement, Name VARCHAR)", 0, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		printTable();
	}



	cout << "INSERT into people (Name) values('Nevo')" << endl;
	rc = sqlite3_exec(db, "INSERT into people (Name) values('Nevo')", NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	
	cout << "INSERT into people (Name) values('Ron')" << endl;
	rc = sqlite3_exec(db, "INSERT into people (Name) values('Ron')", NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	
	cout << "INSERT into people (Name) values('BANANA')" << endl;
	rc = sqlite3_exec(db, "INSERT into people (Name) values('BANANA')", NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	
	
	cout << "UPDATE people SET Name = 'HAKONA_MATATA' WHERE Name = 'BANANA'" << endl;
	rc = sqlite3_exec(db, "UPDATE people SET Name = 'HAKONA_MATATA' WHERE Name = 'BANANA'", NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

}

void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}












